/*
 * precompile.cxx
 *
 * PWLib application source file for openam
 *
 * Precompiled header generation file.
 *
 * Copyright 1999 Equivalence
 *
 * $Log: precompile.cxx,v $
 * Revision 1.1  2007/11/07 03:42:19  willamowius
 * port OpenAM to H323Plus
 *
 * Revision 1.1  1999/10/11 00:35:40  craigs
 * Initiali version
 *
 * Revision 1.2  1999/01/26 06:35:08  robertj
 * Fixed $LOG$ variable in template files
 *
 */

#include <ptlib.h>


// End of File ///////////////////////////////////////////////////////////////
